package com.assignment;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CombinationAssignmentTestNg {
  
	@Test
	  public void customerName() {
		  System.out.println("Naveen");
	  }
	
	@Test
	  public void customername() {
		  System.out.println("Raja");
	  }
	
	@Test
	  public void Customername() {
		  System.out.println("Santhosh");
	  }
	
	@Test
	  public void CUSTOMERNAME() {
		  System.out.println("SALEEM");
	  }
	
	@Test
	  public void CUSTOMERname() {
		  System.out.println("Mohammed");
	  }
	
	@BeforeMethod
	  public void beforeCustomer() {
		  System.out.println("Start Method");
	  }
	  @AfterMethod
	  public void afterCustomer() {
		  System.out.println("Close Method");
	  }
	  @BeforeClass
	  public void beforeclass() {
		  System.out.println("Start Class");
	  }
	  @AfterClass
	  public void afterclass() {
		  System.out.println("Close Class");
	  }
	
	
}

package com.assignments;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ActiTimeAssignment {
	
		static WebDriver driver;
		
		  @BeforeClass
		  public void launchApplication() {
			  
			    driver = new ChromeDriver();
			  
			    driver.manage().window().maximize();
				
				driver.get("https://demo.actitime.com/login.do");
				
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  }
		  
		  @Test(priority = 1)
		  public void checkingPresentText() {
			
			    String expected = "Please identify yourself";
			  
			    String actual_text = driver.findElement(By.xpath("//td[text()='Please identify yourself']")).getText();
			  
			    actual_text.contains(expected);
		}
		  
		  @Test(priority = 2)
		  public void checkImage() {
			
			  boolean actual_image = driver.findElement(By.xpath("//div[@class='atLogoImg']")).isDisplayed();
			  
		     assertEquals(actual_image, true);
		
		  }
		  
		  @AfterClass
		  public void closeBrowser() {
			
			  driver.close();

		}

	}



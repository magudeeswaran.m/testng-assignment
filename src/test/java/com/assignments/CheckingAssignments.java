package com.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckingAssignments {
	
	static WebDriver driver;
	
  @BeforeClass
  public void launchApplication() {
	  
	    driver = new ChromeDriver();
	  
	    driver.manage().window().maximize();
		
		driver.get("https://demo.actitime.com/login.do");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }
  
  @Test(priority = 1)
  public void checkingPresentText() {
	
	  WebElement textCheck = driver.findElement(By.xpath("//td[text()='Please identify yourself']"));
	  
	  boolean displayed = textCheck.isDisplayed();
	  
	  boolean a=true;
	  
	  if(displayed==a)
	  {
		  System.out.println("Please identify yourself text is present");
	  }
	  else
	  {
		  System.out.println("Not present");
	  }
  
}
  
  
  @Test(priority = 2)
  public void checkImage() {
	
	  WebElement logoImage = driver.findElement(By.xpath("//div[@class='atLogoImg']"));
	  
      boolean displayed = logoImage.isDisplayed();
	  
	  boolean a=true;
	  
	  if(displayed==a)
	  {
		  System.out.println("Logo Image is present");
	  }
	  else
	  {
		  System.out.println("Not present");
	  }

}
  
  
}

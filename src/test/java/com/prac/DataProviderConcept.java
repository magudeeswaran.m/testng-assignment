package com.prac;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;


public class DataProviderConcept {

	WebDriver driver;
	
	@BeforeClass
	public void setUp()  {
		
        driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
	}
	
	@org.testng.annotations.Test(dataProvider="DataLogin")
	public void loginTest(String username, String password) {
		
		driver.get("https://demowebshop.tricentis.com/");
		
		WebElement login = driver.findElement(By.linkText("Log in"));
		login.click();
		
		driver.findElement(By.id("Email")).sendKeys(username);
		
		driver.findElement(By.id("Password")).sendKeys(password);
		
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		driver.findElement(By.xpath("//a[text()='Log out']")).click();

	}
	
	@DataProvider(name="DataLogin")
	public String [][] getData() throws IOException {
		File f = new File("/home/magudeeswaran/Documents/Test_data/TestData2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		int norows = sheet.getPhysicalNumberOfRows();
		int noColumns=sheet.getRow(0).getLastCellNum();
		
		String data[][] = new String[norows-1][noColumns];
		
		for(int i=1;i<norows;i++)
		{
			for(int j=0;j<noColumns;j++)
			{
				data[i-1][j] = sheet.getRow(i).getCell(j).getStringCellValue();
			}
		}
return data;
	}
	
	
}

package com.prac;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GroupingConcept {
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  
	  System.out.println("The Customer Will Get Created");
	  
  }
  @Test(groups={"Regression Test"})
  public void newCustomer() {
	  
	  System.out.println("New Customer Will Get Created");
	  
  }
  @Test(groups={"Usability Testing"})
  public void modifyCustomer() {
	  
	  System.out.println("The Customer Will Get Modified");
	  
  }
  @Test(groups={"Smoke Test"})
  public void changeCustomer() {
	  
	  System.out.println("The Customer Will Get Changed");
	  
  }
  @BeforeClass
  public void beforeclass() {
	  System.out.println("Start data base connection , launch browser");
  }
  @AfterClass
  public void afterclass() {
	  System.out.println("Close data base connection , close browser");
  }
  
  
}

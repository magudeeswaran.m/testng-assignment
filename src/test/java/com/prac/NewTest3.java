package com.prac;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest3 {
  
	@Test(dependsOnMethods = "newCustomer")
	  public void modifyCustomer() {
	      System.out.println("THe Customer will get modified");
	  }
	  @Test(dependsOnMethods = "modifyCustomer")
	  public void createCustomer() {
	      System.out.println("THe Customer will get created");
	  }
	  @Test//(dependsOnMethods = "createCustomer")
	  public void newCustomer() {
	      System.out.println("THe New Customer will get created");
	  }
	  @BeforeMethod
	  public void beforeCustomer() {
	      System.out.println("Verifying the customer");
	  }
	  @AfterMethod
	  public void afterCustomer() {
	      System.out.println("all the trancation are done");
	  }
	  @BeforeClass
	  public void beforeClass() {
	      System.out.println("start data connection,launch browser");
	}
	  @AfterClass
	  public void afterClass() {
	      System.out.println("close data connection,closebrowser");
	}
	
}

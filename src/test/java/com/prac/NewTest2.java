package com.prac;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest2 {
	
	@Test
	  public void modifyCustomer() {
	      System.out.println("THe Customer will get modified");
	  }
	  @Test(dependsOnMethods = {"newCustomer","modifyCustomer"})
	  public void createCustomer() {
	      System.out.println("THe Customer will get created");
	  }
	  @Test
	  public void newCustomer() {
	      System.out.println("THe New Customer will get created");
	  }
	  @BeforeMethod
	  public void beforeCustomer() {
	      System.out.println("Verifying the customer");
	  }
	  @AfterMethod
	  public void afterCustomer() {
	      System.out.println("all the trancation are done");
	  }
	  @BeforeClass
	  public void beforeClass() {
	      System.out.println("start data connection,launch browser");
	}
	  @AfterClass
	  public void afterClass() {
	      System.out.println("close data connection,closebrowser");
	}
	
	
	
	
	
	
	
//  @Test
//  public void modifyCustomer() {
//	  System.out.println("The customer got Modified");
//  }
//  @Test(dependsOnMethods = "modifyCustomer")
//  public void createCustomer() {
//	  System.out.println("The customer got created");
//  }
//  @Test(dependsOnMethods = "createCustomer")
//  public void newCustomer() {
//	  System.out.println("The customer got created");
//  }
  
}

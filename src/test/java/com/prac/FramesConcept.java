package com.prac;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FramesConcept {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");

		driver.manage().window().maximize();
		
		driver.switchTo().frame("packageListFrame");
		
		driver.findElement(By.xpath("//ul[@title='Packages']//a[text()='org.openqa.selenium']")).click();
		
		Thread.sleep(3000);
		
		driver.navigate().refresh();
		
		driver.switchTo().frame("packageFrame");
		
		driver.findElement(By.xpath("//a[text()='AbstractHttpCommandCodec']")).click();
		
		Thread.sleep(3000);
		
		driver.navigate().refresh();
		
		driver.switchTo().frame("classFrame");
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//tr[@class='altColor']//a[text()='org.openqa.selenium']")).click();
		
		Thread.sleep(3000);
		
		driver.navigate().refresh();
		
		
	}

}
